
import org.junit.Test;

import hu.braininghub.bh04.api.builder.container.Div;
import hu.braininghub.bh04.api.builder.container.Form;
import hu.braininghub.bh04.api.builder.container.HtmlDocument;
import hu.braininghub.bh04.api.builder.container.Table;
import hu.braininghub.bh04.api.builder.controller.Button;
import hu.braininghub.bh04.api.builder.controller.Input;
import hu.braininghub.bh04.api.builder.controller.InputTypes;
import hu.braininghub.bh04.api.builder.controller.Video;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Attila
 */
public class HtmlDomTest {

	@Test
	public void testHtmlDomWithForm() {

		HtmlDocument root = new HtmlDocument();

		Form f = new Form("main", "/operation", root);

		f.addChild(new Input(InputTypes.text, "t1", "t1", "First name", f));
		f.addChild(new Input(InputTypes.text, "t2", "t1", "Last name", f));
		f.addChild(new Input(InputTypes.text, "t3", "t1", "Address", f));
		root.addChild(f);

		String html = root.getHTMLCodeAsString();

		System.out.println("HTML CODE IS: " + html);

	}

	@Test
	public void testHtmlDocWithTable() {

		HtmlDocument root = new HtmlDocument();
		Table t = new Table("table", 2, 3, new String[] { "A", "B", "C" }, root);

		t.addElementToIndex(0, 0, new Button("b00", "Button00", t));
		t.addElementToIndex(0, 1, new Button("b01", "Button01", t));
		t.addElementToIndex(0, 2, new Button("b02", "Button02", t));
		t.addElementToIndex(0, 2, new Button("b02", "Button02", t));

		t.addElementToIndex(1, 0, new Button("b10", "Button10", t));
		t.addElementToIndex(1, 1, new Button("b11", "Button11", t));
		t.addElementToIndex(1, 2, new Button("b12", "Button12", t));

		String html = t.getHTMLCodeAsString();

		System.out.println("TABLE CODE IS: " + html);

		root.addChild(t);

		html = root.getHTMLCodeAsString();

		System.out.println("HTML CODE IS: " + html);

	}

	@Test
	public void testHtmlDocWithDivWithInsideTable() {

		HtmlDocument root = new HtmlDocument();

		Div d = new Div("tableContainer", root);

		Table t = new Table("table", 2, 3, new String[] { "A", "B", "C" }, d);

		d.addChild(t);

		t.addElementToIndex(0, 0, new Button("b00", "Button00", t));
		t.addElementToIndex(0, 1, new Button("b01", "Button01", t));
		t.addElementToIndex(0, 2, new Button("b02", "Button02", t));
		t.addElementToIndex(0, 2, new Button("b02", "Button02", t));

		t.addElementToIndex(1, 0, new Button("b10", "Button10", t));
		t.addElementToIndex(1, 1, new Button("b11", "Button11", t));
		t.addElementToIndex(1, 2, new Button("b12", "Button12", t));

		String html = t.getHTMLCodeAsString();

		System.out.println("TABLE CODE IS: " + html);

		html = d.getHTMLCodeAsString();

		System.out.println("DIV CODE IS: " + html);

		root.addChild(d);

		html = root.getHTMLCodeAsString();

		System.out.println("HTML CODE IS: " + html);

	}

	@Test
	public void testVideo() {
		HtmlDocument root = new HtmlDocument();

		Video v = new Video("medves", 320, 240, "/src/main/resources/movie.mp4", root);

		System.out.println("Video code:" + v.getHTMLCodeAsString());

		root.addChild(v);
		System.out.println("HTML code:" + root.getHTMLCodeAsString());

	}
}
