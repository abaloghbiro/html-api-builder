/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.html.api.builder.component;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Attila
 */
public abstract class AbstractHtmlContainer extends AbstractHtmlComponent {

    protected List<AbstractHtmlComponent> childComponents = new ArrayList<>();

    public AbstractHtmlContainer(String id) {
        super(id, true);
    }

    public AbstractHtmlContainer(String id, AbstractHtmlContainer parent) {
        super(id, true, parent);
    }

    public void addChild(AbstractHtmlComponent component) {

        if (component.getParentComponent() == null) {
            throw new IllegalArgumentException("Cannot add top level HTML container to an other container!");
        }
        childComponents.add(component);
    }

    public void remove(AbstractHtmlComponent component) {
        if (component.getParentComponent() == null) {
            throw new IllegalArgumentException("Cannot remove top level HTML container from the DOM hierarchy!");
        }
        childComponents.remove(component);
    }
}
