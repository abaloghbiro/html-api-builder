/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.html.api.builder.component;

import java.util.Objects;

/**
 *
 * @author Attila
 */
public abstract class AbstractHtmlComponent {

	protected String id;
	protected String cssClassName;
	protected String style;
	protected boolean isContainer;
	protected final AbstractHtmlComponent parentComponent;
	protected StringBuilder htmlCodeBuilder = new StringBuilder();

	public AbstractHtmlComponent(String id, boolean isContainer) {
		this.id = id;
		this.isContainer = isContainer;
		this.parentComponent = null;
	}

	public AbstractHtmlComponent(String id, boolean isContainer, AbstractHtmlComponent parentComponent) {
		this.id = id;
		this.isContainer = isContainer;
		assert parentComponent != null;
		this.parentComponent = parentComponent;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCssClassName() {
		return cssClassName;
	}

	public void setCssClassName(String cssClassName) {
		this.cssClassName = cssClassName;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public boolean isIsContainer() {
		return isContainer;
	}

	public void setIsContainer(boolean isContainer) {
		this.isContainer = isContainer;
	}

	public AbstractHtmlComponent getParentComponent() {
		return parentComponent;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 47 * hash + Objects.hashCode(this.id);
		hash = 47 * hash + (this.isContainer ? 1 : 0);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final AbstractHtmlComponent other = (AbstractHtmlComponent) obj;
		if (this.isContainer != other.isContainer) {
			return false;
		}
		return Objects.equals(this.id, other.id);
	}

	public abstract String getHTMLCodeAsString();

	@Override
	public String toString() {
		return "HtmlComponent{" + "id=" + id + ", cssClassName=" + cssClassName + ", style=" + style + ", isContainer="
				+ isContainer + '}';
	}
}
