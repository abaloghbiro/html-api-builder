package hu.braininghub.bh04.api.builder.controller;

import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlContainer;
import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlController;

public class Paraghraph extends AbstractHtmlController {

	private Object value;

	public Paraghraph(String id, Object value, AbstractHtmlContainer parent) {
		super(id, parent);
		this.value = value;
	}

	@Override
	public String getHTMLCodeAsString() {
		return "<p>" + value + "</p>";
	}

}
