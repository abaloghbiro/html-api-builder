/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.api.builder.controller;

import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlContainer;
import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlController;

/**
 *
 * @author Attila
 */
public class Video extends AbstractHtmlController {

    private final int width;
    private final int height;
    private final String videoSource;

    // TODO wrong design update it to handle parent components
    public Video(String id, int width, int height, String videoSource, AbstractHtmlContainer parent) {
        super(id,parent);
        this.width = width;
        this.height = height;
        this.videoSource = videoSource;
    }

    @Override
    public String getHTMLCodeAsString() {

        htmlCodeBuilder.append("<video width=\"").append(width).append("\" height=\"").append(height).append("\" controls>");
        htmlCodeBuilder.append("<source src=\"").append(videoSource).append("\">")
                .append("</video>");
        return htmlCodeBuilder.toString();
    }

}
