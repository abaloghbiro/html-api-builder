/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.api.builder.container;

import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlComponent;
import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlContainer;

/**
 *
 * @author Attila
 */
public class Div extends AbstractHtmlContainer {

	public Div(String id, AbstractHtmlContainer parent) {
		super(id, parent);
	}

	@Override
	public String getHTMLCodeAsString() {

		htmlCodeBuilder.append("<div>");

		for (AbstractHtmlComponent c : childComponents) {
			htmlCodeBuilder.append(c.getHTMLCodeAsString());
		}

		return htmlCodeBuilder.append("</div>").toString();
	}

}
