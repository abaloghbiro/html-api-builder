package hu.braininghub.bh04.api.builder.container;

public enum FormMethod {

	GET, POST;
}
