package hu.braininghub.bh04.api.builder.container;

import java.util.UUID;

import hu.braininghub.bh04.api.builder.controller.Br;
import hu.braininghub.bh04.api.builder.controller.Input;
import hu.braininghub.bh04.api.builder.controller.InputTypes;
import hu.braininghub.bh04.api.builder.controller.Label;
import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlComponent;
import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlContainer;

public class Form extends AbstractHtmlContainer {

	private String url;
	private FormMethod method;

	public Form(String id, String url, AbstractHtmlContainer parent) {
		super(id, parent);
		this.url = url;
		this.method = FormMethod.POST;
	}

	public Form(String id, String url, FormMethod method, AbstractHtmlContainer parent) {
		super(id, parent);
		this.url = url;
		this.method = method;
	}

	@Override
	public String getHTMLCodeAsString() {

		htmlCodeBuilder.append("<form action=\"").append(url).append("\"").append(" method=\"").append(method)
				.append("\"").append(">");

		for (AbstractHtmlComponent c : childComponents) {
			Label label = new Label(UUID.randomUUID().toString(), c.getId(), this);
			htmlCodeBuilder.append(label.getHTMLCodeAsString());
			htmlCodeBuilder.append(c.getHTMLCodeAsString());
			htmlCodeBuilder.append(new Br(this).getHTMLCodeAsString());
		}
		htmlCodeBuilder.append(new Input(InputTypes.submit, UUID.randomUUID().toString(), "send", "Send", this).getHTMLCodeAsString());
		htmlCodeBuilder.append("</form>");

		return htmlCodeBuilder.toString();
	}

}
