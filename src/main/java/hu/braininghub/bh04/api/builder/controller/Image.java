package hu.braininghub.bh04.api.builder.controller;

import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlContainer;
import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlController;

public class Image extends AbstractHtmlController {

	private String sourceOfImage;
	private String altText;
	private int height;
	private int width;

	public Image(String id, AbstractHtmlContainer parent) {
		super(id, parent);
	}

	@Override
	public String getHTMLCodeAsString() {

		return htmlCodeBuilder.append("<img").append("src=").append("\"").append(sourceOfImage).append("alt=").append(altText)
				.append("height=").append(height).append("width=").append(width).append(">").toString();

	}

	public String getSourceOfImage() {
		return sourceOfImage;
	}

	public void setSourceOfImage(String sourceOfImage) {
		this.sourceOfImage = sourceOfImage;
	}

	public String getAltText() {
		return altText;
	}

	public void setAltText(String altText) {
		this.altText = altText;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

}
