package hu.braininghub.bh04.api.builder.controller;

import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlContainer;
import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlController;

public class Label extends AbstractHtmlController {

	private String labelFor;
	private String value;

	public Label(String id, String labelFor, AbstractHtmlContainer parent) {
		super(id, parent);
		this.labelFor = labelFor;
		this.value = labelFor;
	}

	public Label(String id, String labelFor, String value, AbstractHtmlContainer parent) {
		super(id, parent);
		this.labelFor = labelFor;
		this.value = value;
	}

	@Override
	public String getHTMLCodeAsString() {

		super.toString();

		htmlCodeBuilder.append("<label for=\"").append(labelFor).append("\"").append(">").append(value)
				.append("</label>");

		return htmlCodeBuilder.toString();
	}

}
