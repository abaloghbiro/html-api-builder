/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.api.builder.controller;

import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlContainer;
import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlController;

/**
 *
 *
 * @author Attila
 */
public class Br extends AbstractHtmlController {

	public Br(AbstractHtmlContainer parent) {
		super(null, parent);
	}

	@Override
	public String getHTMLCodeAsString() {
		return "<br>";
	}

}
